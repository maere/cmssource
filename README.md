# Group Project: Content Management System #

To get this project up and running you will need to compile and set up the database from the database script.

### What is this repository for? ###

* This was our final project (team of 4) for our Java training at The Software Craftsmanship Guild 
* Version 1: Basic CRUD functionality and web design for blog posts, including images and TinyMCE integration.


### How do I get set up? ###

* Dependencies: Dependencies are built into the POM.xml file
* Database configuration: We used mySQL. There is a database setup script included.
* How to run tests: Import project into a Java IDE and compile.  Open test file, right click, select test.
* Deployment instructions: You are on your own with that.  :)

### Contribution guidelines ###

* Not currently accepting contributions since this is a demo project.